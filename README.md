## About Project
```bash
This is a Book Webapp to manage admin and user portal.
Book webapp is developed in Laravel using Daisy ui and tailwind css.
```

## Installation

- Make sure your setup is pointed to `/public` directory
- `git clone git@bitbucket.org:kishan_rank_hauper/book-webapp.git`
- Copy `.env.example` to `.env` then set your database connection and SMTP details
- Run `composer install` command
- Run `php artisan key:generate` commands
- Run `php artisan migrate --seed` command
- Delete `public/storage` folder and run `php artisan storage:link`
- Run `npm install` command
- Run `npm install -D tailwindcss postcss autoprefixer` command
- Run `npx tailwindcss init` command
- Run `npm run dev` command
- Run `npm run watch` command

## Credentials
- Home Page  : http://127.0.0.1:8000/
- Login Page : http://127.0.0.1:8000/login

-User Credentials :
    Email : user@gmail.com
    Password : password

-Admin Credentials :
    Email : admin@gmail.com
    Password : password