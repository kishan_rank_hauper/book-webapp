<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::whereEmail('admin@gmail.com')->first();
        $user = User::whereEmail('user@gmail.com')->first();

        if (!$admin) {
            User::create([
                'name' => 'Hauper Admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('password'),
                'role' => User::ROLE_ADMIN
            ]);
        }

        if (!$user) {
            User::create([
                'name' => 'Hauper User',
                'email' => 'user@gmail.com',
                'password' => bcrypt('password'),
                'role' => User::ROLE_USER
            ]);
        }

        Book::factory()->count(20)->create();
    }
}
